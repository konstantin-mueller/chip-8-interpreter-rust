pub struct FpsCounter {
    now: u64,
    last: u64,
    delta: u64,
    fps: u32,
}

impl FpsCounter {
    pub fn new() -> FpsCounter {
        let now;
        unsafe {
            now = sdl2::sys::SDL_GetPerformanceCounter();
        }

        FpsCounter {
            now,
            last: now,
            delta: 0,
            fps: 0,
        }
    }

    pub fn update(&mut self) {
        unsafe {
            self.now = sdl2::sys::SDL_GetPerformanceCounter();
            self.delta = (self.now - self.last) * 1000 / sdl2::sys::SDL_GetPerformanceFrequency();
        }

        if self.delta > 1000 {
            self.last = self.now;
            println!("FPS: {}", self.fps);
            self.fps = 0;
        } else {
            self.fps += 1;
        }
    }
}
