extern crate rand;

use std::{fs, io::Read, num::Wrapping};

use rand::Rng;

const FONTSET: [u8; 80] = [
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80, // F
];

enum ConditionOperation {
    Equals,
    NotEquals,
}

fn load_game(path: &str) -> std::io::Result<Vec<u8>> {
    let mut file = fs::File::open(path)?;

    let mut buffer: Vec<u8> = Vec::with_capacity(file.metadata()?.len() as usize + 1);
    file.read_to_end(&mut buffer)?;

    Ok(buffer)
}

pub struct Chip8 {
    opcode: u16,
    memory: [u8; 4096],
    v_registers: [Wrapping<u8>; 16],
    i_register: u16,
    program_counter: u16,
    pub screen: [u8; 64 * 32],
    delay_timer: u8,
    sound_timer: u8,
    stack: Vec<u16>,
    pub keys: [u8; 16],
    pub draw_flag: bool,
}

impl Chip8 {
    pub fn new(game_path: &str) -> Chip8 {
        let mut memory: [u8; 4096] = [0; 4096];
        for (i, char) in FONTSET.iter().enumerate() {
            memory[i] = *char;
        }

        let buffer = load_game(game_path).unwrap();
        for (i, byte) in buffer.iter().enumerate() {
            // Game memory begins at 0x200 which is 512
            memory[i + 512] = *byte;
        }

        Chip8 {
            opcode: 0,
            memory,
            v_registers: [Wrapping(0); 16],
            i_register: 0,
            program_counter: 0x200,
            screen: [0; 64 * 32],
            delay_timer: 0,
            sound_timer: 0,
            stack: Vec::new(),
            keys: [0; 16],
            draw_flag: false,
        }
    }

    pub fn emulate_cycle(&mut self) {
        let pc = self.program_counter as usize;
        // Fetch opcode:
        // Shift first 8 bits in a u16 to the left (adds 8 zeroes to the right of the 8 bits)
        // Then use bitwise OR to merge the first 8 bits with the next 8 bits
        // This creates the 2 byte opcode used by CHIP-8
        self.opcode = (self.memory[pc] as u16) << 8 | self.memory[pc + 1] as u16;

        // Decode opcode:
        // For opcodes in the first 4 bits
        match self.opcode & 0xF000 {
            // For opcodes not in the first 4 bits but in the last 4 bits instead but starting with 0
            0x0000 => match self.opcode & 0x000F {
                0x0000 => self.clear(),
                0x000E => self.program_counter = self.stack.pop().unwrap() + 2, //return
                _ => self.print_error(Some("0x0000")),
            },
            0x1000 => self.program_counter = self.opcode & 0x0FFF, // goto (jump)
            0x2000 => self.call_subroutine(),
            0x3000 => self.condition(ConditionOperation::Equals, self.opcode & 0x00FF),
            0x4000 => self.condition(ConditionOperation::NotEquals, self.opcode & 0x00FF),
            0x5000 => self.condition(
                ConditionOperation::Equals,
                self.v_registers[((self.opcode & 0x00F0) >> 4) as usize].0 as u16,
            ),
            0x6000 => self.set_v_register(),
            0x7000 => self.add_to_v_register(),
            0x8000 => match self.opcode & 0x000F {
                0x0000 => self.set_v_register_to_other_v_value(),
                0x0001 => self.set_v_register_to_self_or_other_v_value(),
                0x0002 => self.set_v_register_to_self_and_other_v_value(),
                0x0003 => self.set_v_register_to_self_xor_other_v_value(),
                0x0004 => self.add_v_registers(),
                0x0005 => self.substract_v_registers_x_y(),
                0x0006 => self.shift_v_register_right(),
                0x0007 => self.substract_v_registers_y_x(),
                0x000E => self.shift_v_register_left(),
                _ => self.print_error(Some("0x8000")),
            },
            0x9000 => self.condition(
                ConditionOperation::NotEquals,
                self.v_registers[((self.opcode & 0x00F0) >> 4) as usize].0 as u16,
            ),
            0xA000 => self.set_i_register(),
            0xB000 => {
                // goto (jump)
                self.program_counter =
                    (self.v_registers[0].0 as u16).wrapping_add(self.opcode & 0x0FFF);
            }
            0xC000 => self.set_v_register_to_random(),
            0xD000 => self.draw(),
            0xE000 => match self.opcode & 0x00FF {
                0x009E => {
                    // Skips next instruction if key in VX is pressed
                    self.program_counter += if self.keys
                        [self.v_registers[((self.opcode & 0x0F00) >> 8) as usize].0 as usize]
                        != 0
                    {
                        4
                    } else {
                        2
                    }
                }
                0x00A1 => {
                    // Skips next instruction if key in VX is not pressed
                    self.program_counter += if self.keys
                        [self.v_registers[((self.opcode & 0x0F00) >> 8) as usize].0 as usize]
                        == 0
                    {
                        4
                    } else {
                        2
                    }
                }
                _ => self.print_error(Some("0xE000")),
            },
            0xF000 => match self.opcode & 0x00FF {
                0x0007 => self.set_v_register_to_delay_timer(),
                0x000A => self.wait_for_key_press(),
                0x0015 => self.set_delay_timer(),
                0x0018 => self.set_sound_timer(),
                0x001E => self.add_v_register_to_i(),
                0x0029 => self.set_i_register_to_sprite_location_for_v_character(),
                0x0033 => self.save_decimal_v_in_i(),
                0x0055 => self.store_or_fill_v0_to_vx_starting_at_i(true),
                0x0065 => self.store_or_fill_v0_to_vx_starting_at_i(false),
                _ => self.print_error(Some("0xF000")),
            },
            _ => self.print_error(None),
        }

        self.update_timers();
    }

    /** 00E0 */
    fn clear(&mut self) {
        for i in 0..(64 * 32) {
            self.screen[i] = 0x0;
        }
        self.draw_flag = true;
        self.program_counter += 2;
    }

    /** 2NNN */
    fn call_subroutine(&mut self) {
        self.stack.push(self.program_counter);
        self.program_counter = self.opcode & 0x0FFF;
    }

    /** 3XNN, 4XNN, 5XY0, 9XY0 */
    fn condition(&mut self, operation: ConditionOperation, compare_v_with: u16) {
        let v = self.v_registers[((self.opcode & 0x0F00) >> 8) as usize].0 as u16;

        self.program_counter += match operation {
            ConditionOperation::Equals => {
                if v == compare_v_with {
                    4
                } else {
                    2
                }
            }
            ConditionOperation::NotEquals => {
                if v != compare_v_with {
                    4
                } else {
                    2
                }
            }
        }
    }

    /** 6XNN */
    fn set_v_register(&mut self) {
        self.v_registers[((self.opcode & 0x0F00) >> 8) as usize] =
            Wrapping((self.opcode & 0x00FF) as u8);
        self.program_counter += 2;
    }

    /** 7XNN */
    fn add_to_v_register(&mut self) {
        self.v_registers[((self.opcode & 0x0F00) >> 8) as usize] +=
            Wrapping((self.opcode & 0x00FF) as u8);
        self.program_counter += 2;
    }

    /** 8XY0 */
    fn set_v_register_to_other_v_value(&mut self) {
        self.v_registers[((self.opcode & 0x0F00) >> 8) as usize] =
            self.v_registers[((self.opcode & 0x00F0) >> 4) as usize];
        self.program_counter += 2;
    }

    /** 8XY1 */
    fn set_v_register_to_self_or_other_v_value(&mut self) {
        self.v_registers[((self.opcode & 0x0F00) >> 8) as usize] |=
            self.v_registers[((self.opcode & 0x00F0) >> 4) as usize];
        self.program_counter += 2;
    }

    /** 8XY2 */
    fn set_v_register_to_self_and_other_v_value(&mut self) {
        self.v_registers[((self.opcode & 0x0F00) >> 8) as usize] &=
            self.v_registers[((self.opcode & 0x00F0) >> 4) as usize];
        self.program_counter += 2;
    }

    /** 8XY3 */
    fn set_v_register_to_self_xor_other_v_value(&mut self) {
        self.v_registers[((self.opcode & 0x0F00) >> 8) as usize] ^=
            self.v_registers[((self.opcode & 0x00F0) >> 4) as usize];
        self.program_counter += 2;
    }

    /** 8XY4 */
    fn add_v_registers(&mut self) {
        let first_index = ((self.opcode & 0x0F00) >> 8) as usize;
        let second = self.v_registers[((self.opcode & 0x00F0) >> 4) as usize];

        // if first + second > 255 => there is a carry => set flag
        self.v_registers[0xF] = Wrapping(
            if self.v_registers[first_index]
                .0
                .checked_add(second.0)
                .is_none()
            {
                1
            } else {
                0
            },
        );

        self.v_registers[first_index] += second;
        self.program_counter += 2;
    }

    /** 8XY5 */
    fn substract_v_registers_x_y(&mut self) {
        let first_index = ((self.opcode & 0x0F00) >> 8) as usize;
        let second = self.v_registers[((self.opcode & 0x00F0) >> 4) as usize];

        // if second > first => there is a borrow => set flag
        self.v_registers[0xF] = Wrapping(if second > self.v_registers[first_index] {
            0
        } else {
            1
        });

        self.v_registers[first_index] -= second;
        self.program_counter += 2;
    }

    /** 8XY6 */
    fn shift_v_register_right(&mut self) {
        self.v_registers[0xF] =
            self.v_registers[((self.opcode & 0x0F00) >> 8) as usize] & Wrapping(0x1);
        self.v_registers[((self.opcode & 0x0F00) >> 8) as usize] >>= 1;
        self.program_counter += 2;
    }

    /** 8XY7 */
    fn substract_v_registers_y_x(&mut self) {
        let first_index = ((self.opcode & 0x0F00) >> 8) as usize;
        let second = self.v_registers[((self.opcode & 0x00F0) >> 4) as usize];

        // if second > first => there is a borrow => set flag
        self.v_registers[0xF] = Wrapping(if self.v_registers[first_index] > second {
            0
        } else {
            1
        });

        self.v_registers[first_index] = second - self.v_registers[first_index];
        self.program_counter += 2;
    }

    /** 8XYE */
    fn shift_v_register_left(&mut self) {
        self.v_registers[0xF] = self.v_registers[((self.opcode & 0x0F00) >> 8) as usize] >> 7;
        self.v_registers[((self.opcode & 0x0F00) >> 8) as usize] <<= 1;
        self.program_counter += 2;
    }

    /** ANNN */
    fn set_i_register(&mut self) {
        self.i_register = self.opcode & 0x0FFF;
        self.program_counter += 2;
    }

    /** CXNN */
    fn set_v_register_to_random(&mut self) {
        self.v_registers[((self.opcode & 0x0F00) >> 8) as usize] =
            Wrapping(rand::thread_rng().gen_range(0..=255) & (self.opcode & 0x00FF) as u8);
        self.program_counter += 2;
    }

    /** DXYN */
    fn draw(&mut self) {
        let x = self.v_registers[((self.opcode & 0x0F00) >> 8) as usize].0 as usize;
        let y = self.v_registers[((self.opcode & 0x00F0) >> 4) as usize].0 as usize;
        let height = (self.opcode & 0x000F) as u8;
        let mut pixel: u8;

        self.v_registers[0xF] = Wrapping(0);
        // Loop over rows
        for yline in 0..height as usize {
            pixel = self.memory[(self.i_register + yline as u16) as usize];

            // Loop over 8 bits of one row
            for xline in 0..8 {
                if pixel & (0x80 >> xline) != 0 {
                    let screen_index = ((x + xline) % 64) + ((y + yline) % 32) * 64;

                    // Is pixel already 1?
                    if self.screen[screen_index] == 1 {
                        self.v_registers[0xF] = Wrapping(1);
                    }

                    // Set pixel value with XOR
                    self.screen[screen_index] ^= 1;
                }
            }
        }
        self.draw_flag = true;
        self.program_counter += 2;
    }

    /** FX07 */
    fn set_v_register_to_delay_timer(&mut self) {
        self.v_registers[((self.opcode & 0x0F00) >> 8) as usize] = Wrapping(self.delay_timer);
        self.program_counter += 2;
    }

    /** FX0A */
    fn wait_for_key_press(&mut self) {
        let i = self
            .keys
            .iter()
            .enumerate()
            .find_map(|(i, key)| if *key != 0 { Some(i) } else { None });

        // Only continue if any key was pressed. Otherwise try again next cycle
        if i.is_some() {
            self.v_registers[((self.opcode & 0x0F00) >> 8) as usize] = Wrapping(i.unwrap() as u8);
            self.program_counter += 2;
        }
    }

    /** FX15 */
    fn set_delay_timer(&mut self) {
        self.delay_timer = self.v_registers[((self.opcode & 0x0F00) >> 8) as usize].0;
        self.program_counter += 2;
    }

    /** FX18 */
    fn set_sound_timer(&mut self) {
        self.sound_timer = self.v_registers[((self.opcode & 0x0F00) >> 8) as usize].0;
        self.program_counter += 2;
    }

    /** FX1E */
    fn add_v_register_to_i(&mut self) {
        self.i_register = self.v_registers[((self.opcode & 0x0F00) >> 8) as usize]
            .0
            .into();
        self.program_counter += 2;
    }

    /** FX29 */
    fn set_i_register_to_sprite_location_for_v_character(&mut self) {
        self.i_register = self.v_registers[((self.opcode & 0x0F00) >> 8) as usize].0 as u16 * 0x5;
        self.program_counter += 2;
    }

    /**
    FX33

    Takes the decimal representation of VX, places the hundreds digit in memory
    at location in I, the tens digit at location I+1, and the ones digit at
    location I+2
    */
    fn save_decimal_v_in_i(&mut self) {
        let v = self.v_registers[((self.opcode & 0x0F00) >> 8) as usize].0;
        let i = self.i_register as usize;

        self.memory[i] = v / 100;
        self.memory[i + 1] = v / 10 % 10;
        self.memory[i + 2] = v % 100 % 10;
        self.program_counter += 2;
    }

    /** FX55, FX65 */
    fn store_or_fill_v0_to_vx_starting_at_i(&mut self, store: bool) {
        let i_register = self.i_register as usize;

        for i in 0..((self.opcode & 0x0F00) >> 8) as usize {
            if store {
                self.memory[i_register + i] = self.v_registers[i].0;
            } else {
                self.v_registers[i] = Wrapping(self.memory[i_register + i]);
            }
        }

        self.i_register += ((self.opcode & 0x0F00) >> 8) + 1;
        self.program_counter += 2;
    }

    fn update_timers(&mut self) {
        if self.delay_timer > 0 {
            self.delay_timer -= 1;
        }

        if self.sound_timer > 0 {
            if self.sound_timer == 1 {
                // TODO: play sound instead
                println!("BEEP!")
            }
            self.sound_timer -= 1;
        }
    }

    fn print_error(&self, instruction: Option<&str>) {
        println!(
            "Unknown opcode [{}]: 0x{:X}",
            instruction.unwrap_or(""),
            self.opcode
        )
    }
}
