mod chip8;
mod fps_counter;
mod render;
mod resource;

use chip8::Chip8;
use fps_counter::FpsCounter;

use sdl2::event::Event::{KeyDown, KeyUp};
use sdl2::keyboard::Keycode;

macro_rules! match_keys {
    ($event:expr, $break_main:block, $chip8:expr, $($key_code:path, $key_index:expr),+) => {
        match $event {
            sdl2::event::Event::Quit { .. } => $break_main,
            $(
                KeyDown {keycode: Some($key_code), ..} => $chip8.keys[$key_index] = 1,
                KeyUp {keycode: Some($key_code), ..} => $chip8.keys[$key_index] = 0,
            )+
            _ => {}
        }
    };
}

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() < 2 {
        println!("The first parameter is expected to be a path to a CHIP8 application");
        std::process::exit(1);
    }

    let mut graphics = render::Graphics::new();
    let mut chip8 = Chip8::new(&args[1]);
    let mut fps = FpsCounter::new();

    'main: loop {
        fps.update();

        for event in graphics.event_pump.poll_iter() {
            match_keys!(
                event,
                {
                    break 'main;
                },
                chip8,
                Keycode::Num1,
                0,
                Keycode::Num2,
                1,
                Keycode::Num3,
                2,
                Keycode::Num4,
                3,
                Keycode::Q,
                4,
                Keycode::W,
                5,
                Keycode::E,
                6,
                Keycode::R,
                7,
                Keycode::A,
                8,
                Keycode::S,
                9,
                Keycode::D,
                10,
                Keycode::F,
                11,
                Keycode::Y,
                12,
                Keycode::X,
                13,
                Keycode::C,
                14,
                Keycode::V,
                15
            );
        }

        chip8.emulate_cycle();

        if chip8.draw_flag {
            graphics.update(&chip8);
        }
    }
}
