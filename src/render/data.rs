#[derive(Debug, Clone, Copy)]
#[repr(C, packed)]
pub struct Vec2 {
    pub x: f32,
    pub y: f32,
}

impl Vec2 {
    pub fn new(x: f32, y: f32) -> Vec2 {
        Vec2 { x, y }
    }
}

#[derive(Debug, Clone, Copy)]
#[repr(C, packed)]
pub struct Vec3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Vec3 {
    pub fn new(x: f32, y: f32, z: f32) -> Vec3 {
        Vec3 { x, y, z }
    }

    pub unsafe fn vertex_attrib_pointer(stride: usize, location: usize, offset: Option<usize>, size: Option<i32>) {
        let index = location as gl::types::GLuint;
        let pointer = offset.unwrap_or(0);

        gl::EnableVertexAttribArray(index);
        gl::VertexAttribPointer(
            index,
            size.unwrap_or(3),
            gl::FLOAT,
            gl::FALSE,
            stride as gl::types::GLint,
            pointer as *const gl::types::GLvoid,
        );
    }
}

impl From<(f32, f32, f32)> for Vec3 {
    fn from(v: (f32, f32, f32)) -> Self {
        Vec3::new(v.0, v.1, v.2)
    }
}
