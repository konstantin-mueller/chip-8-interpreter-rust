use std;
use std::ffi::{CStr, CString};

use gl;
use nalgebra_glm as glm;
use thiserror::Error;

use crate::resource::{self, Resource};

#[derive(Error, Debug)]
pub enum Error {
    #[error("Failed to load file '{name}'")]
    LoadFile {
        name: String,
        #[source]
        inner: resource::Error,
    },
    #[error("The shader type for the file '{name}' is invalid")]
    InvalidShaderType { name: String },
    #[error("Failed to compile shader '{name}'. Reason: {message}")]
    CompileShader { name: String, message: String },
    #[error("Failed to link shader program '{name}'. Reason: {message}")]
    LinkShader { name: String, message: String },
}

pub struct Shader {
    id: gl::types::GLuint,
}

impl Shader {
    pub fn from_source(source: &CStr, shader_type: gl::types::GLenum) -> Result<Shader, String> {
        let id = shader_from_source(source, shader_type)?;
        Ok(Shader { id })
    }

    pub fn from_resource(resource: &Resource, name: &str) -> Result<Shader, Error> {
        const POSSIBLE_EXTENSIONS: [(&str, gl::types::GLenum); 2] =
            [(".vert", gl::VERTEX_SHADER), (".frag", gl::FRAGMENT_SHADER)];

        let shader_type = POSSIBLE_EXTENSIONS
            .iter()
            .find(|&&(extension, _)| name.ends_with(extension))
            .map(|&(_, s_type)| s_type)
            .ok_or(Error::InvalidShaderType { name: name.into() })?;

        let source = resource
            .read_file_into_cstring(name)
            .map_err(|error| Error::LoadFile {
                name: name.into(),
                inner: error,
            })?;

        Shader::from_source(&&source, shader_type).map_err(|message| Error::CompileShader {
            name: name.into(),
            message,
        })
    }

    pub fn id(&self) -> gl::types::GLuint {
        self.id
    }
}

impl Drop for Shader {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteShader(self.id);
        }
    }
}

pub struct Program {
    id: gl::types::GLuint,
}

impl Program {
    pub fn from_shaders(shaders: &[Shader]) -> Result<Program, String> {
        let id = unsafe { gl::CreateProgram() };

        for shader in shaders {
            unsafe { gl::AttachShader(id, shader.id()) }
        }

        unsafe {
            gl::LinkProgram(id);
        }

        let mut success: gl::types::GLint = 1;
        unsafe {
            gl::GetProgramiv(id, gl::LINK_STATUS, &mut success);
        }

        if success == 0 {
            let mut len: gl::types::GLint = 0;
            unsafe {
                gl::GetProgramiv(id, gl::INFO_LOG_LENGTH, &mut len);
            }

            let error = create_cstring_with_spaces(len as usize);

            unsafe {
                gl::GetProgramInfoLog(
                    id,
                    len,
                    std::ptr::null_mut(),
                    error.as_ptr() as *mut gl::types::GLchar,
                );
            }

            return Err(error.to_string_lossy().into_owned());
        }

        for shader in shaders {
            unsafe {
                gl::DetachShader(id, shader.id);
            }
        }

        Ok(Program { id })
    }

    pub fn from_resource(resource: &Resource, name: &str) -> Result<Program, Error> {
        const POSSIBLE_EXTENSIONS: [&str; 2] = [".vert", ".frag"];

        let shaders = POSSIBLE_EXTENSIONS
            .iter()
            .map(|extension| Shader::from_resource(resource, &format!("{}{}", name, extension)))
            .collect::<Result<Vec<Shader>, Error>>()?;

        Program::from_shaders(&shaders[..]).map_err(|message| Error::LinkShader {
            name: name.into(),
            message,
        })
    }

    pub fn use_program(&self) {
        unsafe {
            gl::UseProgram(self.id);
        }
    }

    pub fn set_int(&self, name: &str, value: i32) {
        let name_cstr = CString::new(name.as_bytes()).unwrap();

        unsafe {
            gl::Uniform1i(gl::GetUniformLocation(self.id, name_cstr.as_ptr()), value);
        }
    }

    pub fn set_uniform_mat4(&self, name: &str, matrix: &glm::Mat4) {
        let name_cstr = CString::new(name.as_bytes()).unwrap();

        unsafe {
            gl::UniformMatrix4fv(
                gl::GetUniformLocation(self.id, name_cstr.as_ptr() as *const gl::types::GLchar),
                1,
                gl::FALSE,
                glm::value_ptr(matrix).as_ptr() as *const gl::types::GLfloat,
            );
        }
    }
}

impl Drop for Program {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteProgram(self.id);
        }
    }
}

fn shader_from_source(
    source: &CStr,
    shader_type: gl::types::GLenum,
) -> Result<gl::types::GLuint, String> {
    let id = unsafe { gl::CreateShader(shader_type) };

    unsafe {
        gl::ShaderSource(id, 1, &source.as_ptr(), std::ptr::null());
        gl::CompileShader(id);
    }

    let mut success: gl::types::GLint = 1;
    unsafe {
        gl::GetShaderiv(id, gl::COMPILE_STATUS, &mut success);
    }

    if success == 0 {
        let mut len: gl::types::GLint = 0;
        unsafe {
            gl::GetShaderiv(id, gl::INFO_LOG_LENGTH, &mut len);
        }

        let error = create_cstring_with_spaces(len as usize);

        // Write error message from OpenGL into error
        unsafe {
            gl::GetShaderInfoLog(
                id,
                len,
                std::ptr::null_mut(),
                error.as_ptr() as *mut gl::types::GLchar,
            );
        }

        // Return error Result with CString converted to a Rust String. into_owned is
        // called to ensure that a String is returned and no &str.
        return Err(error.to_string_lossy().into_owned());
    }

    Ok(id)
}

fn create_cstring_with_spaces(len: usize) -> CString {
    // Create buffer with ASCII space bytes based on len
    let mut buffer: Vec<u8> = Vec::with_capacity(len as usize + 1);
    buffer.extend([b' '].iter().cycle().take(len as usize));
    // Convert buffer to CString
    unsafe { CString::from_vec_unchecked(buffer) }
}
