use gl;

use super::data;

#[derive(Debug, Clone, Copy)]
#[repr(C, packed)]
pub struct Vertex {
    pub pos: data::Vec3,
    pub color: data::Vec3,
    pub tex_coords: data::Vec2,
}

impl Vertex {
    pub fn vertex_attrib_pointers() {
        let stride = std::mem::size_of::<Self>();

        unsafe {
            // Location 0 in vertex shader for vertices
            data::Vec3::vertex_attrib_pointer(stride, 0, None, None);

            // Location 1 in vertex shader for colors
            data::Vec3::vertex_attrib_pointer(
                stride,
                1,
                Some(std::mem::size_of::<data::Vec3>()),
                None,
            );

            // Location 2 for texture
            data::Vec3::vertex_attrib_pointer(
                stride,
                2,
                Some(6 * std::mem::size_of::<f32>()),
                Some(2),
            );
        }
    }
}

pub trait BufferType {
    const BUFFER_TYPE: gl::types::GLuint;
}

pub struct BufferTypeArray;
impl BufferType for BufferTypeArray {
    const BUFFER_TYPE: gl::types::GLuint = gl::ARRAY_BUFFER;
}

pub struct BufferTypeElementArray;
impl BufferType for BufferTypeElementArray {
    const BUFFER_TYPE: gl::types::GLuint = gl::ELEMENT_ARRAY_BUFFER;
}

pub type ArrayBuffer = Buffer<BufferTypeArray>;
pub type ElementArrayBuffer = Buffer<BufferTypeElementArray>;

/// Contains the vbo (vertex buffer object) to load vertices into OpenGL
pub struct Buffer<T>
where
    T: BufferType,
{
    vbo: gl::types::GLuint,
    _marker: std::marker::PhantomData<T>,
}

impl<T> Buffer<T>
where
    T: BufferType,
{
    pub fn new() -> Buffer<T> {
        let mut vbo: gl::types::GLuint = 0;
        unsafe {
            gl::GenBuffers(1, &mut vbo);
        }

        Buffer {
            vbo,
            _marker: std::marker::PhantomData,
        }
    }

    pub fn bind(&self) {
        unsafe {
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);
        }
    }

    pub fn unbind(&self) {
        unsafe {
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
        }
    }

    pub fn load_data<U>(&self, data: &[U]) {
        unsafe {
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (data.len() * std::mem::size_of::<U>()) as gl::types::GLsizeiptr,
                data.as_ptr() as *const gl::types::GLvoid,
                gl::STATIC_DRAW,
            );
        }
    }
}

impl<T> Drop for Buffer<T>
where
    T: BufferType,
{
    fn drop(&mut self) {
        unsafe {
            gl::DeleteBuffers(1, &mut self.vbo);
        }
    }
}

/// Contains the vao (vertex array object) to describe to OpenGL how to interpret the
/// data in the buffer and convert it to inputs for the vertex shader.
pub struct VertexArray {
    vao: gl::types::GLuint,
}

impl VertexArray {
    pub fn new() -> VertexArray {
        let mut vao: gl::types::GLuint = 0;
        unsafe {
            gl::GenVertexArrays(1, &mut vao);
        }
        VertexArray { vao }
    }

    pub fn bind(&self) {
        unsafe {
            gl::BindVertexArray(self.vao);
        }
    }

    pub fn unbind(&self) {
        unsafe {
            gl::BindVertexArray(0);
        }
    }
}

impl Drop for VertexArray {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteVertexArrays(1, &mut self.vao);
        }
    }
}
