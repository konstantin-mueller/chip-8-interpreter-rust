use gl;

pub struct Viewport {
    pub x: i32,
    pub y: i32,
    pub width: i32,
    pub height: i32,
}

impl Viewport {
    pub fn for_window(width: i32, height: i32) -> Viewport {
        Viewport {
            x: 0,
            y: 0,
            width,
            height,
        }
    }

    pub fn update(&mut self, width: i32, height: i32) {
        self.width = width;
        self.height = height;

        self.set_gl_viewport();
    }

    pub fn set_gl_viewport(&self) {
        unsafe {
            gl::Viewport(self.x, self.y, self.width, self.height);
        }
    }
}
