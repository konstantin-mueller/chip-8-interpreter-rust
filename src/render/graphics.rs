use std::{convert::TryInto, path::Path};

use nalgebra_glm as glm;

use crate::{
    chip8::Chip8,
    render::{
        self,
        data::{Vec2, Vec3},
    },
    resource::Resource,
};

use super::{
    buffer::{ArrayBuffer, Vertex, VertexArray},
    Program,
};

const WINDOW_WIDTH: u32 = 640;
const WINDOW_HEIGHT: u32 = 320;
const TEX_WIDTH: i32 = 64;
const TEX_HEIGHT: i32 = 32;

pub struct Graphics {
    pub event_pump: sdl2::EventPump,
    pub window: sdl2::video::Window,
    viewport: super::Viewport,
    color_buffer: super::ColorBuffer,
    screen_buffer: [[[u8; 3]; TEX_WIDTH as usize]; TEX_HEIGHT as usize],
    shader_program: Program,
    vao: VertexArray,
    texture: u32,
    _gl_context: sdl2::video::GLContext,
}

impl Graphics {
    pub fn new() -> Graphics {
        let sdl = sdl2::init().unwrap();
        let video_subsystem = sdl.video().unwrap();

        let gl_attr = video_subsystem.gl_attr();
        gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
        gl_attr.set_context_version(4, 1);

        let window = video_subsystem
            .window("CHIP-8", WINDOW_WIDTH, WINDOW_HEIGHT)
            .opengl()
            .position_centered()
            .resizable()
            .build()
            .unwrap();
        let _gl_context = window.gl_create_context().unwrap();

        gl::load_with(|s| video_subsystem.gl_get_proc_address(s) as *const std::os::raw::c_void);

        let viewport = render::Viewport::for_window(WINDOW_WIDTH as i32, WINDOW_HEIGHT as i32);
        viewport.set_gl_viewport();

        let color_buffer = render::ColorBuffer::from_color(glm::vec3(0.0, 0.0, 0.0));

        let resource = Resource::from_relative_exe_path(Path::new("assets")).unwrap();
        let shader_program = Program::from_resource(&resource, "shaders/texture").unwrap();

        let (texture, screen_buffer) = init_texture().unwrap();
        shader_program.use_program();
        shader_program.set_int("tex", 0);

        Graphics {
            event_pump: sdl.event_pump().unwrap(),
            window,
            viewport,
            color_buffer,
            screen_buffer,
            shader_program,
            vao: init_quad(),
            texture,
            _gl_context,
        }
    }

    pub fn resize(&mut self, width: i32, height: i32) {
        self.viewport.update(width, height);
    }

    pub fn update(&mut self, chip8: &Chip8) {
        self.color_buffer.clear_gl_color_buffer_bit();

        for y in 0..32 {
            for x in 0..64 {
                let color = if chip8.screen[y * 64 + x] == 0 {
                    0
                } else {
                    255
                };

                self.screen_buffer[y][x][0] = color;
                self.screen_buffer[y][x][1] = color;
                self.screen_buffer[y][x][2] = color;
            }
        }

        unsafe {
            gl::TexSubImage2D(
                gl::TEXTURE_2D,
                0,
                0,
                0,
                TEX_WIDTH,
                TEX_HEIGHT,
                gl::RGB,
                gl::UNSIGNED_BYTE,
                self.screen_buffer.as_ptr() as *const gl::types::GLvoid,
            );
        }

        self.shader_program.use_program();
        self.vao.bind();

        unsafe {
            gl::ActiveTexture(gl::TEXTURE0);
            gl::BindTexture(gl::TEXTURE_2D, self.texture);
            gl::DrawArrays(gl::TRIANGLES, 0, 6);
        }

        self.window.gl_swap_window();
    }
}

fn init_quad() -> VertexArray {
    let vbo = ArrayBuffer::new();
    vbo.bind();

    let color: Vec3 = (1.0, 1.0, 1.0).into();
    vbo.load_data(&vec![
        Vertex {
            pos: (-1.0, -1.0, 0.0).into(),
            color,
            tex_coords: Vec2::new(0.0, 0.0),
        },
        Vertex {
            pos: (1.0, -1.0, 0.0).into(),
            color,
            tex_coords: Vec2::new(1.0, 0.0),
        },
        Vertex {
            pos: (1.0, 1.0, 0.0).into(),
            color,
            tex_coords: Vec2::new(1.0, 1.0),
        },
        Vertex {
            pos: (-1.0, 1.0, 0.0).into(),
            color,
            tex_coords: Vec2::new(0.0, 1.0),
        },
        Vertex {
            pos: (-1.0, -1.0, 0.0).into(),
            color,
            tex_coords: Vec2::new(0.0, 0.0),
        },
        Vertex {
            pos: (1.0, 1.0, 0.0).into(),
            color,
            tex_coords: Vec2::new(1.0, 1.0),
        },
    ]);

    let vao = VertexArray::new();
    vao.bind();

    Vertex::vertex_attrib_pointers();

    vbo.unbind();
    vao.unbind();

    vao
}

fn init_texture(
) -> Result<(u32, [[[u8; 3]; TEX_WIDTH as usize]; TEX_HEIGHT as usize]), <u16 as TryInto<u8>>::Error>
{
    let screen_buffer = [[[0; 3]; TEX_WIDTH as usize]; TEX_HEIGHT as usize];
    let mut texture: u32 = 0;

    unsafe {
        gl::GenTextures(1, &mut texture);
        gl::BindTexture(gl::TEXTURE_2D, texture);

        gl::TexParameteri(
            gl::TEXTURE_2D,
            gl::TEXTURE_MAG_FILTER,
            gl::NEAREST.try_into()?,
        );
        gl::TexParameteri(
            gl::TEXTURE_2D,
            gl::TEXTURE_MIN_FILTER,
            gl::NEAREST.try_into()?,
        );
        gl::TexParameteri(
            gl::TEXTURE_2D,
            gl::TEXTURE_WRAP_S,
            gl::CLAMP_TO_BORDER.try_into()?,
        );
        gl::TexParameteri(
            gl::TEXTURE_2D,
            gl::TEXTURE_WRAP_T,
            gl::CLAMP_TO_BORDER.try_into()?,
        );

        gl::TexImage2D(
            gl::TEXTURE_2D,
            0,
            gl::RGB.try_into().unwrap(),
            TEX_WIDTH,
            TEX_HEIGHT,
            0,
            gl::RGB,
            gl::UNSIGNED_BYTE,
            screen_buffer.as_ptr() as *const gl::types::GLvoid,
        );
    }

    Ok((texture, screen_buffer))
}
