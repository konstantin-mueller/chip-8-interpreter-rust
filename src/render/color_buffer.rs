use gl;
use nalgebra_glm as glm;

pub struct ColorBuffer {
    pub color: glm::Vec4,
}

impl ColorBuffer {
    pub fn from_color(color: glm::Vec3) -> ColorBuffer {
        let color_buffer = ColorBuffer {
            color: color.fixed_resize(1.0),
        };
        color_buffer.set_color();
        color_buffer
    }

    pub fn update(&mut self, color: glm::Vec3) {
        self.color = color.fixed_resize(1.0);
        self.set_color();
    }

    pub fn clear_gl_color_buffer_bit(&self) {
        unsafe {
            gl::Clear(gl::COLOR_BUFFER_BIT);
        }
    }

    fn set_color(&self) {
        unsafe {
            gl::ClearColor(self.color.x, self.color.y, self.color.z, self.color.w);
        }
    }
}
