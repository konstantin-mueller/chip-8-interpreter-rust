use std::ffi;
use std::fs;
use std::io::{self, Read};
use std::path::{Path, PathBuf};

use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("I/O error")]
    Io {
        #[from]
        source: io::Error,
    },
    #[error("Failed to read a file that contains a null byte (\\0)")]
    FileContainsNullByte,
    #[error("Failed to get executeable path")]
    FailedToGetExePath,
}

pub struct Resource {
    root_path: PathBuf,
}

impl Resource {
    pub fn from_relative_exe_path(rel_path: &Path) -> Result<Resource, Error> {
        let exe_file_name = std::env::current_exe().map_err(|_| Error::FailedToGetExePath)?;
        let exe_path = exe_file_name.parent().ok_or(Error::FailedToGetExePath)?;

        Ok(Resource {
            root_path: exe_path.join(rel_path),
        })
    }

    pub fn read_file_into_cstring(&self, resource_name: &str) -> Result<ffi::CString, Error> {
        let mut file = fs::File::open(to_platform_specific_separated_path(
            &self.root_path,
            resource_name,
        ))?;

        // Allocate buffer for file (+1 byte for the \0 string terminator byte that
        // is expected by C for strings)
        let mut buffer: Vec<u8> = Vec::with_capacity(file.metadata()?.len() as usize + 1);
        file.read_to_end(&mut buffer)?;

        if buffer.iter().find(|b| **b == 0).is_some() {
            return Err(Error::FileContainsNullByte);
        }

        Ok(unsafe { ffi::CString::from_vec_unchecked(buffer) })
    }
}

fn to_platform_specific_separated_path(root_dir: &Path, location: &str) -> PathBuf {
    let mut path: PathBuf = root_dir.into();

    for part in location.split("/") {
        path = path.join(part);
    }

    path
}
