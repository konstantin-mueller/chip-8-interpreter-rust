#version 410 core

layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 color;
layout (location = 2) in vec2 i_tex_coord;

out vec3 v_color;
out vec2 tex_coord;

void main()
{
    gl_Position = vec4(pos, 1.0f);
    v_color = color;
    tex_coord = i_tex_coord;
}
