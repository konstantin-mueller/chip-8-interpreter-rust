#version 410 core

in vec3 v_color;
in vec2 tex_coord;

out vec4 color;

uniform sampler2D tex;

void main()
{
    color = texture(tex, tex_coord);
}
